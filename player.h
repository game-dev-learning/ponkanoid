#ifndef ARKANOID_PLAYER_H
#define ARKANOID_PLAYER_H

#include "raylib.h"

class Player
{
private:
    Vector2 position;
    Vector2 size;
    Color color = {242, 108, 79, 255};
    float speed = 0.0f;

    // 3 frames to get maxSpeed
    const float acceleration = 3.34f;
    const float damping = 0.5f;
    const float maxSpeed = 10.0f;

public:
    Player(float posX, float posY, float width, float height);

    void draw();
    void moveUp();
    void moveDown();
    void damp();
    void update();
};

#endif