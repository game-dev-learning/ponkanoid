#include <iostream>
#include <string>
#include "raylib.h"
#include "player.h"

int main(int argc, char **argv)
{
    // This means 4:3
    const float aspectRatio = 3.0f / 4.0f;
    const int screenWidth = 480;
    const int screenHeight = screenWidth * aspectRatio;

    Player player(135.0f, 196.0f, 12.0f, 50.0f);

    InitWindow(screenWidth, screenHeight, "Baisic window");
    SetTargetFPS(60);

    while (!WindowShouldClose())
    {
        if (IsKeyDown(KEY_UP))
        {
            player.moveUp();
        }
        if (IsKeyDown(KEY_DOWN))
        {
            player.moveDown();
        }
        if (IsKeyUp(KEY_UP) && IsKeyUp(KEY_DOWN))
        {
            player.damp();
        }

        player.update();

        BeginDrawing();
        ClearBackground(RAYWHITE);
        DrawFPS(0, 0);
        player.draw();
        EndDrawing();
    }

    CloseWindow();

    return 0;
}