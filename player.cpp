#include <iostream>
#include <string>
#include "player.h"

Player::Player(float posX, float posY, float width, float height)
{
    position = {posX, posY};
    size = {width, height};
}

void Player::draw()
{
    DrawRectangleV(position, size, color);
    const std::string spdText = "SPEED: " + std::to_string(speed);
    DrawText(spdText.c_str(), 0, 25, 20, LIME);
}

void Player::moveUp()
{
    speed -= acceleration;
    if (speed < -maxSpeed)
        speed = -maxSpeed;
}

void Player::moveDown()
{
    speed += acceleration;
    if (speed > maxSpeed)
        speed = maxSpeed;
}

void Player::damp()
{
    speed = speed * (1.0f - damping);
    if (speed > -0.5f && speed < 0.5f)
        speed = 0;
}

void Player::update()
{
    position.y += speed;
    if (position.y < 0)
        position.y = 0;
}